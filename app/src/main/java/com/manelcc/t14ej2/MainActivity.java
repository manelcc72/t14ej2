package com.manelcc.t14ej2;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int HELLO_ID = 0;
    private Button btn_create_notification, bt_erase_notification, bt_list_notification, bt_access_notification;
    private TextView tv_notification;
    private NotificationReceiver nReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btn_create_notification = (Button) findViewById(R.id.btn_create_notification);
        bt_erase_notification = (Button) findViewById(R.id.bt_erase_notification);
        bt_list_notification = (Button) findViewById(R.id.bt_list_notification);
        bt_access_notification = (Button) findViewById(R.id.bt_access_notification);
        tv_notification = (TextView) findViewById(R.id.tv_notification);

        btn_create_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(MainActivity.this);

                mBuilder.setContentTitle("Mi notificacion");
                mBuilder.setContentText("Contenido");
                mBuilder.setTicker("notificacion listener example");
                mBuilder.setSmallIcon(R.mipmap.ic_launcher);
                mBuilder.setAutoCancel(true);
                mBuilder.setTicker("Notificación");


                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(HELLO_ID, mBuilder.build());
            }
        });

        bt_access_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
            }
        });

        bt_erase_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBroadcast(new Intent("com.imaginagroup.NOTIFICATION_LISTENER_SERVICE_EXAMPLE")
                        .putExtra("command", "clearall"));
            }
        });

        bt_list_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBroadcast(new Intent("com.imaginagroup.NOTIFICATION_LISTENER_SERVICE_EXAMPLE")
                        .putExtra("command", "list"));
            }
        });


        nReceiver = new NotificationReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.imaginagroup.NOTIFICATION_LISTENER_EXAMPLE");
        registerReceiver(nReceiver, filter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(nReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class NotificationReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String temp = intent.getStringExtra("notification_event") + "\n" + tv_notification.getText();
            tv_notification.setText(temp);
        }
    }
}
